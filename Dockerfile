FROM tomcat:8.5.51
RUN apt-get update
COPY /target  /usr/local/tomcat/webapps
EXPOSE 8080
